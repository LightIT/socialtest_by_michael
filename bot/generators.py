import string
import random


def _get_random_string(base, length=10):
    random_string = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(length)])
    return '{}_{}'.format(base, random_string)


def generate_username(user_num):
    return _get_random_string('u_{}'.format(user_num))


def generate_email(user_num):
    random_part = _get_random_string('em_{}'.format(user_num), 6)
    return "{}@gmail.com".format(random_part)


def generate_password():
    return _get_random_string('p', 6)


def generate_title():
    return _get_random_string('title', 5)


def generate_text():
    return _get_random_string('text', 20)
