import random
import bot.generators as generator
from .exceptions import NoUsersToLikeException, NoPostsToLikeException
from .client import Client
from .bot_logger import logger

class User:
    def __init__(self, user_id, email, username, password):
        self.id = user_id
        self.token = ''
        self.email = email
        self.username = username
        self.password = password
        self.posts = []
        self.liked_posts = []
        self.likes_count = 0
        self.can_like = True

    def __repr__(self):
        return "User(id={}, username={})".format(self.id, self.username)


class Manager:

    def __init__(self, config_data):
        self.client = Client()
        self.NUM_OF_USERS = config_data['number_of_users']
        self.MAX_POSTS_PER_USER = config_data['max_posts_per_user']
        self.MAX_LIKES_PER_USER = config_data['max_likes_per_user']
        self.posts_to_like = True
        self.users_can_like = True
        self.users = dict()

    @classmethod
    def logging_message(cls, msg):
        length = 120
        spaces = (length - len(msg)) // 2
        if len(msg) % 2 == 1:
            return '{}{}{}'.format('-' * spaces, msg, '-' * (spaces + 1))
        return '{}{}{}'.format('-' * spaces, msg, '-' * spaces)

    def _make_auth_headers(self, user):
        return {'Authorization': 'JWT {}'.format(user.token)}

    def create_user(self, user_num):
        email = generator.generate_email(user_num)
        username = generator.generate_username(user_num)
        password = generator.generate_password()
        data = {
            'email': email,
            'username': username,
            'password': password
        }
        response_data = self.client.create_user(data)
        user_id = response_data.get('id')
        logger.info(Manager.logging_message('CREATED USER - ' + username))
        user = User(user_id, email, username, password)
        return user

    def login_user(self, user):
        data = {
            'username': user.username,
            'password': user.password
        }
        response_data = self.client.login_user(data)
        user.token = response_data.get('token')

    def pick_user_for_liking(self):
        for user in sorted(self.users.values(), key=lambda x: len(x.posts), reverse=True):
            if user.likes_count < self.MAX_LIKES_PER_USER and user.can_like:
                logger.info(Manager.logging_message('Picked user - {}'.format(user)))
                return user
        raise NoUsersToLikeException

    def populate_user_posts(self, user):
        headers = self._make_auth_headers(user)
        response_data = self.client.user_detail(user_id=user.id, headers=headers)
        user.posts = response_data.get('posts')

    def create_post(self, user):
        headers = self._make_auth_headers(user)
        title = generator.generate_title()
        text = generator.generate_text()
        data = {
            'title': title,
            'text': text,
        }
        self.client.create_post(headers=headers, data=data)
        logger.info(Manager.logging_message('CREATED POST by - ' + user.username))

    def pick_post_for_like(self, user):
        posts = self.client.post_list()
        self.check_posts_for_empty_likes(posts)
        post_author = None
        for p in posts:
            if len(p.get('likes')) == 0 and p.get('id') not in user.posts:
                post_author = p.get('author')
        if not post_author:
            return None
        author = self.users.get(post_author)
        logger.info(Manager.logging_message('ONE POST FROM {} WILL BE LIKED'.format(author)))
        return random.choice(author.posts)

    def check_posts_for_empty_likes(self, posts):
        if not any([p for p in posts if len(p.get('likes')) == 0]):
            raise NoPostsToLikeException

    def liking_activity(self, user):
        while user.likes_count < self.MAX_LIKES_PER_USER:
            post = self.pick_post_for_like(user)
            if not post:
                user.can_like = False
                return
            logger.info(Manager.logging_message('PICKED POST - {}'.format(post)))
            if post in user.liked_posts:
                logger.info(Manager.logging_message('ALREADY LIKED'.format(post)))
                continue
            self.like_post(user, post)

    def like_post(self, user, post):
        headers = self._make_auth_headers(user)
        self.client.like_post(post_id=post, headers=headers)
        user.liked_posts.append(post)
        user.likes_count += 1
        logger.info(Manager.logging_message('LIKED POST - {} by {}'.format(post, user)))
