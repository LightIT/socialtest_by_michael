
class NoUsersToLikeException(Exception):
    pass


class NoPostsToLikeException(Exception):
    pass
