from urllib.parse import urljoin
import requests


class Client:
    BASE_URL = 'http://localhost:8000/api/'

    USER_LOGIN_URL = 'auth/login/'
    USERS_URL = 'users/'
    POSTS_URL = 'posts/'
    POST_LIKE_URL = 'posts/{}/like/'

    def _do_request(self, request_method, uri, headers=None, data=None):
        url = self.BASE_URL + uri
        r = requests.request(request_method, url=url, headers=headers, data=data)
        r.raise_for_status()
        return r.json()

    def create_user(self, data):
        return self._do_request('post', uri=self.USERS_URL, data=data)

    def login_user(self, data):
        return self._do_request('post', uri=self.USER_LOGIN_URL, data=data)

    def user_detail(self, user_id, headers):
        url = urljoin(self.USERS_URL, str(user_id))
        return self._do_request('get', uri=url, headers=headers)

    def create_post(self, headers, data):
        return self._do_request('post', uri=self.POSTS_URL, headers=headers, data=data)

    def post_list(self):
        return self._do_request('get', uri=self.POSTS_URL)

    def like_post(self, post_id, headers):
        uri = self.POST_LIKE_URL.format(post_id)
        return self._do_request('post', uri=uri, headers=headers)
