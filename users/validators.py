from django.conf import settings
from rest_framework import serializers

from users.utils import get_data_from_api


class HunterMailValidator(object):

    def __call__(self, value):
        if not self._check_email(value):
            message = "Sorry we can't create user with this email. Please try another."
            raise serializers.ValidationError(message)

    def _check_email(self, value):
        base_url = settings.EMAILHUNTER_VERIFY_URL
        query_params = {
            "email": value,
            "api_key": settings.EMAILHUNTER_API_KEY
        }
        emailhunter_data = get_data_from_api(base_url, query_params)
        if emailhunter_data:
            deliverable = emailhunter_data.get('result', None)
            webmail = emailhunter_data.get('webmail', None)
            return False if deliverable == 'undeliverable' and not webmail else True
