from django.conf import settings

import requests
import clearbit


def get_data_from_api(base_url, query_params):
    r = requests.get(base_url, params=query_params)
    r.raise_for_status()
    data = r.json()
    return data


def get_data_from_clearbit(email):
    clearbit.key = settings.CLEARBIT_SECRET_KEY
    clearbit_data = clearbit.Enrichment.find(email=email)
    first_name = clearbit_data['person']['name']['givenName']
    last_name = clearbit_data['person']['name']['familyName']
    return first_name, last_name
