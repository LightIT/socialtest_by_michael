from django.contrib.auth import get_user_model

from celery import task

from users.utils import get_data_from_clearbit


@task
def populate_clearbit_data(user_pk):
    if not user_pk:
        return
    user = get_user_model().objects.get(pk=user_pk)
    email = user.email
    first_name, last_name = get_data_from_clearbit(email=email)
    if first_name:
        user.first_name = first_name
    if last_name:
        user.last_name = last_name
    user.save()
