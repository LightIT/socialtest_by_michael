from django.db.models.signals import post_save
from django.dispatch import receiver

from .tasks import populate_clearbit_data
from .models import User


@receiver(post_save, sender=User)
def user_post_save(sender, created, instance, *args, **kwargs):
    if created:
        populate_clearbit_data.delay(instance.pk)
