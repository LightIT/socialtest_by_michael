1. Clone project.
2. Create virtual environment.
3. Install requirements, Redis.
4. Create db.
5. Migrate `./manage.py migrate`.
6. Run Redis server.
7. Run server `./manage.py runserver`.
8. Run celery `celery -A social_test worker -l info `.
9. Run bot `python bot.py`.
