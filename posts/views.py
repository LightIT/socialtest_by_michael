from rest_framework import permissions
from rest_framework import status
from rest_framework.decorators import detail_route
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from rest_framework.viewsets import ModelViewSet

from .models import Post, Like
from .exceptions import LikeException
from .serializers import PostSerializer


class PostViewSet(ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    @detail_route(methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def like(self, request, pk=None):
        post = get_object_or_404(Post, pk=pk)
        user = self.request.user
        try:
            like = Like.perform_action(liked_by=user, post=post)
        except LikeException as e:
            return Response({"error": e.message}, status=status.HTTP_400_BAD_REQUEST)
        if like:
            data = {'status': 'Liked'}
            return Response(status=status.HTTP_201_CREATED, data=data)
        else:
            data = {'status': 'Unliked'}
            return Response(status=status.HTTP_204_NO_CONTENT, data=data)
