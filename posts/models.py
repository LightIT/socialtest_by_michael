from django.conf import settings
from django.db import models

from .exceptions import LikeException


class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='posts')

    title = models.CharField(max_length=50)
    text = models.CharField(max_length=400)

    def __str__(self):
        return '{} by {}'.format(self.title, self.author)


class Like(models.Model):
    liked_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='likes')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='likes')

    def __str__(self):
        return 'Like {} from {}'.format(self.post, self.liked_by)

    @classmethod
    def perform_action(cls, liked_by, post):
        if post.author == liked_by:
            raise LikeException("User can't like his own posts")
        like, created = Like.objects.get_or_create(liked_by=liked_by, post=post)
        if not created:
            like.delete()
            return None
        return like
