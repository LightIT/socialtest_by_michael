from django.conf.urls import url, include
from django.contrib import admin
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [url(r'^admin/', admin.site.urls),
               url(r'^api/', include('users.urls')),
               url(r'^api/', include('posts.urls')),
               url(r'^api/auth/login/', obtain_jwt_token), ]
