import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'social_test.settings')

app = Celery('social_test')
app.config_from_object('social_test.celery_config')
app.autodiscover_tasks()



