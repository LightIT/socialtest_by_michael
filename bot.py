import json
import random
import sys

from bot.manager import Manager
from bot.bot_logger import logger
from bot.exceptions import NoPostsToLikeException, NoUsersToLikeException




def read_config_data():
    logger.info(Manager.logging_message('GET CONFIG DATA'))
    with open('bot/config.json') as data_file:
        try:
            data = json.load(data_file)
        except:
            logger.fatal('Bad config file. Exit!')
            sys.exit()
    for item in data:
        if not isinstance(data[item], int) or data[item] < 0:
            logger.fatal('Bad config file data. Only positive numbers as values. Exit!')
            sys.exit()
    return data


class Bot:
    def __init__(self, config_data):
        self.manager = Manager(config_data)

    def initialize_users(self):
        for i in range(self.manager.NUM_OF_USERS):
            user = self.manager.create_user(i)
            self.manager.users[user.id] = user

    def initialize_posts(self):
        for user in self.manager.users.values():
            self.manager.login_user(user)
            posts_count = random.randint(1, self.manager.MAX_POSTS_PER_USER)
            logger.info(Manager.logging_message('{} - generate {} posts'.format(user, posts_count)))
            for i in range(posts_count):
                self.manager.create_post(user)
            self.manager.populate_user_posts(user)

    def run(self):
        self.initialize_users()
        self.initialize_posts()
        while True:
            try:
                user = self.manager.pick_user_for_liking()
                logger.info(Manager.logging_message('STARTING LIKING ACTIVITY'))
                self.manager.liking_activity(user)
            except NoPostsToLikeException:
                logger.info(Manager.logging_message('No posts to like. Stopping'))
                sys.exit()
            except NoUsersToLikeException:
                logger.info(Manager.logging_message('No users to like. Stopping'))
                sys.exit()
            except Exception as e:
                logger.info('Something bad')
                logger.info(e)
                sys.exit()


if __name__ == '__main__':
    logger.info(Manager.logging_message('STARTING BOT'))
    config_data = read_config_data()
    bot = Bot(config_data)
    bot.run()
